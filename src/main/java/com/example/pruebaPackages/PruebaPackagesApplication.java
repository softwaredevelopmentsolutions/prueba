package com.example.pruebaPackages;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PruebaPackagesApplication {

	public static void main(String[] args) {
		SpringApplication.run(PruebaPackagesApplication.class, args);
	}

}
